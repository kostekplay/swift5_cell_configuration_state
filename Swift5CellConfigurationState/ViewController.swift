////  ViewController.swift
//  Swift5CellConfigurationState
//
//  Created on 05/11/2020.
//  
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    let tableView: UITableView = {
       
        let tv = UITableView()
        tv.register(MyTableViewCell.self, forCellReuseIdentifier: MyTableViewCell.identifier)
        return tv
        
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        view.addSubview(tableView)
        tableView.frame = view.bounds
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MyTableViewCell.identifier, for: indexPath) as? MyTableViewCell ?? UITableViewCell()
        
        return cell
    }

}

