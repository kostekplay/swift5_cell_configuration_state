////  MyTableViewCell.swift
//  Swift5CellConfigurationState
//
//  Created on 05/11/2020.
//  
//

import UIKit

class MyTableViewCell: UITableViewCell {

    static let identifier = "MyTableViewCell"
   
    override func updateConfiguration(using state: UICellConfigurationState) {
        super.updateConfiguration(using: state)
        
        var configuration = defaultContentConfiguration().updated(for: state)
        configuration.text = "Hello !"
        configuration.image = UIImage(systemName: "bell")
        
        var backgroundConfig = backgroundConfiguration?.updated(for: state)
        backgroundConfig?.backgroundColor = .systemGray
        
        if state.isHighlighted || state.isSelected {
            configuration.textProperties.color = .red
            configuration.imageProperties.tintColor = .yellow
            backgroundConfig?.backgroundColor = .systemBlue
        }
        
        contentConfiguration = configuration
        backgroundConfiguration = backgroundConfig
    }
}
